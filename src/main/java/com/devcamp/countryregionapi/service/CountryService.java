package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Country;

@Service
public class CountryService {
    // @Autowired  chỉ được thực hiện trong phương thức . ở bên ngoài không thực hiện được
    @Autowired  // gọi tới 1 đối tượng (bean) khác mà không cần khởi tạo nữa

    
    private RegionService regionService;

    Country vietnam = new Country("VN", "Vietnam" );
    Country us = new Country("US", "MY  " );
    Country russia = new Country("RUS", "NGA" );

    // phương thức trả về  Arrlist tất cả các quốc gia
    public ArrayList<Country> getAllcountries() {
        ArrayList<Country> allCountry = new ArrayList<Country>();
        vietnam.setRegions(regionService.getRegionsVietNam());
        us.setRegions(regionService.getRegionsUS());
        russia.setRegions(regionService.getRegionsMC());


        allCountry.add(vietnam);
        allCountry.add(us);
        allCountry.add(russia);
        return allCountry;

    }
}
